//LOCALHOST

$(document).ready(function () {

    var BB = document.getElementsByClassName("BB");
    console.log("BB= " + BB);
    var BR = document.getElementsByClassName("BR");
    var BT = document.getElementsByClassName("BT");

    var stitchX = 0;  //initialisation de x et y 
    var stitchY = 0;


    var table = [
        [BB, BR, BR, 0, BB, 0, BR, 0],
        [BR, BR, BB, BR, BB, BB, BB, 0],
        [0, BR, 0, BB, BB, BB, BB, 0],
        [BB, 0, BR, BR, 0, BR, BR, 0],
        [0, 0, BR, BR, BR, BR, BR, BT],
        [0, 0, 0, 0, 0, 0, BB, BT]
    ];


    //on récupère l'image stitch du html

    var stitch = document.querySelector("#stitch");




    var Rows = table[0].length;  //nombres de lignes
    console.log(Rows);

    var numColumn = table.length; //nombre de colonnes

    console.log(numColumn);



    var TOP = 38, BOTTOM = 40, LEFT = 37, RIGHT = 39; //stocker les numeros des touches


    $(document).keydown(function (e) {
        var stitchwin = $('.col-' + stitchY + '-' + stitchX);
        console.log("stitchwin ");
        console.log(stitchwin);
        var chi = $(".chi");

        //si stitch n'est pas dans la case de Chi

        if (!stitchwin.hasClass('chi')) {
            var winner = $(".winner").toggle(false);

            switch (e.keyCode) {

                case BOTTOM:  //touche du bas = Stitch descend d'une case
                    console.log(stitch);


                    if (stitchX <= Rows - 2) { //si stitch n'est pas en bout de ligne = +1case en bas

                        //On recupère la position pour y intégrer stitch
                        var stitchHTML = $('.col-' + stitchY + '-' + stitchX);

                        var stitchB = $('.col-' + stitchY + '-' + (stitchX += 1));
                        console.log(stitchB);

                        if (!stitchHTML.hasClass('BB') && !stitchB.hasClass('BT')) {
                            // stitchX += 1;
                            let stitchHTML = $('.col-' + stitchY + '-' + stitchX);
                            console.log(stitchHTML);
                            stitchHTML.append(stitch);
                            // console.log(stitch);
                        }
                        else {
                            stitchX -= 1;
                            //alert("stop");
                            break;

                        }
                    }
                    else { //si stitch sort du labyrinthe on alert
                        //alert("trop bas!")
                        break;
                    }
                    console.log("B " + stitchX);
                    break;

                case TOP: // touche du haut on monte d'une case 

                    if (stitchX > 0) {
                        var stitchHTML = $('.col-' + stitchY + '-' + stitchX);
                        var stitchT = $('.col-' + stitchY + '-' + (stitchX -= 1));

                        if (!stitchHTML.hasClass('BT') && !stitchT.hasClass('BB')) {
                            //stitchX -= 1;
                            let stitchHTML = $('.col-' + stitchY + '-' + stitchX);
                            console.log(stitchHTML + " blabla")
                            stitchHTML.append(stitch);
                            console.log(stitchHTML);
                            console.log(stitch);
                        }
                        else {
                            stitchX += 1;
                            //alert("stop");
                            break;
                        }
                    }

                    else {
                        //  alert("trop haut!");
                        break;
                    }
                    console.log("T " + stitchX);
                    break;

                case LEFT: //touche de gauche on se decale vers la gauche

                    if (stitchY > 0) {

                        var stitchHTML = $('.col-' + stitchY + '-' + stitchX);
                        var stitchL = $('.col-' + (stitchY -= 1) + '-' + stitchX);

                        if (!stitchL.hasClass('BR')) {
                            //stitchY -= 1;
                            let stitchHTML = $('.col-' + stitchY + '-' + stitchX);
                            console.log(stitchHTML + " blabla")
                            stitchHTML.append(stitch);
                            console.log(stitchHTML);
                            console.log(stitch);
                        }
                        else {
                            stitchY += 1;
                            // alert("stop");
                            break;
                        }

                    }
                    else {
                        // alert("plus de place de ce côté!");
                        break;
                    }
                    console.log("L " + stitchY);
                    break;

                case RIGHT: //touche de droite on se décale vers la droite
                
                    startChrono();

                    if (stitchY < numColumn - 1) {

                        var stitchHTML = $('.col-' + stitchY + '-' + stitchX);
                        var stitchR = $('.col-' + (stitchY += 1) + '-' + stitchX);

                        if (!stitchHTML.hasClass("BR") && !stitchR.hasClass('BL')) {
                            //stitchY +=1;
                            let stitchHTML = $('.col-' + stitchY + '-' + stitchX);
                            console.log(stitchHTML + " blabla");
                            stitchHTML.append(stitch);
                            console.log(stitchHTML);
                            console.log(stitch);
                        }
                        else {
                            stitchY -= 1;
                            // alert("stop");
                            break;
                        }

                        console.log("R " + stitchY);
                        break;
                    }
                    else {
                        //  alert("plus de place");
                        break;
                    }
            }

        } else {
            stopChrono();
            var winner = $(".winner").toggle(true);

            console.log("winner " + winner);
            $("#reset").click(function () {
                document.location.reload(true);
                /* stitchX = 0;
                 stitchY = 0;
                 resetChrono();
                 var winner = $(".winner").toggle(false);
                 var stitchHTML = $('.col-' + stitchY + '-' + stitchX);
                 stitchHTML.append(stitch);
                 startChrono();*/

            })
        }
    });


    // ----------------------------------- CHRONO -------------------------------------------------

    var firstStart = true;
    var a;
    var time = 0; // secondes

    var secHTML = document.querySelector(".secondes");
    var minHTML = document.querySelector(".minutes");
    //var chrono = $('#timer');

    function updateHTMLChrono() {

        secHTML.innerHTML = time % 60;
        minHTML.innerHTML = Math.floor(time / 60);

    }

    function updateChrono() {
        time++;


        updateHTMLChrono();
    }

    function startChrono() {
        if (firstStart == true) {
            a = setInterval(updateChrono, 10);
            firstStart = false;
        }
    };
    //startChrono();


    function stopChrono() {
        clearInterval(a);
    }

    /*function resetChrono(){
        time=0;
        secHTML.innerHTML=0;
        minHTML.innerHTML=0;
        id=true;
    }
    */

    /*-----------------------------------------------------------------*/


});